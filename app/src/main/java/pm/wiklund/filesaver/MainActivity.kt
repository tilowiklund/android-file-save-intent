package pm.wiklund.filesaver

import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toFile
import java.io.IOException
import java.nio.file.Files
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {
    private val FILE_DIR_REQUEST_CODE = 2;

    fun truncateString(str : String, max : Int, marker : String = "...", front : Boolean = false) : String {
        when {
            str.length <= max -> {
                return str
            }
            front -> {
                return marker + str.takeLast(max - marker.length)
            }
            else -> {
                return str.take(max - marker.length) + marker
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        when {
            intent?.action == Intent.ACTION_SEND -> {
                val stream = intent?.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri
                val text = intent?.getStringExtra(Intent.EXTRA_TEXT)

                val textpreview = findViewById<TextView>(R.id.textpreview)
                val textsave = findViewById<Button>(R.id.savetext)
                if (text != null) {
                    textsave.isEnabled = true
                    textpreview.setText(truncateString(text, 15))
                } else {
                    textsave.isEnabled = false
                    textpreview.setText("No text included")
                }

                val filepreview = findViewById<TextView>(R.id.filepreview)
                val filesave = findViewById<Button>(R.id.savefile)
                if (stream != null) {
                    filepreview.setText(truncateString(stream.toString(), 15, front = true))
                    filesave.isEnabled = true

                    filesave.setOnClickListener {
                        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
                            if(!granted) {
                                Toast.makeText(this, "Unable to get permission!", Toast.LENGTH_LONG).show()
                            } else {
                                registerForActivityResult(ActivityResultContracts.CreateDocument()) { uri : Uri ->
                                    val inputStream = contentResolver.openInputStream(stream)
                                    if(inputStream == null) {
                                        throw IOException("Unable to open input stream!")
                                    } else {
                                        val outputStream = contentResolver.openOutputStream(uri)
                                        if(outputStream == null) {
                                            throw IOException("Unable to open output stream!")
                                        } else {
                                            inputStream.copyTo(outputStream)
                                            outputStream.close()
                                        }
                                        inputStream.close()
                                    }
                                }.launch(stream.pathSegments?.last()?.toString())
                            }
                        }.launch(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    }

                } else {
                    filesave.isEnabled = false
                    filepreview.setText("No file included")
                }
            }
        }
    }
}
